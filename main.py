# This is a sample Python script.
import sys
import unittest
import guiserver.guiserver
from server.server import Server
from server.standaloneConnection import StandaloneConnection


def run_server(debug=False):
    crawlerServer = Server()
    crawlerServer.run()
    guiserver.guiserver.run_flask(debug, 'guiserver/static', crawlerServer)


def run_standalone(debug=False):
    crawlerServer = Server()

    sc = StandaloneConnection()
    sc.connectWorker("", "", "")
    sc.worker.registerAllDefaultPlugins()

    crawlerServer.add_worker(sc)
    crawlerServer.run()
    guiserver.guiserver.run_flask(debug, 'guiserver/static', crawlerServer)


mode = -1

if __name__ == '__main__':
    if mode == -1:
        print("Welcome to web crawler app! Please specify which configuration to choose!")
        print("[0] - standalone")
        print("[1] - server")
        print("[2] - worker")
        print("[3] - test")
        print("[4] - server in debug (requires app reboot)")
        print("[5] - standalone in debug (requires app reboot)")

        mode = int(input())  # 0 - standalone; 1 - server; 2 - worker

    if mode == 0:
        run_standalone()
    elif mode == 1:
        run_server()
    elif mode == 2:
        pass
    elif mode == 3:
        testsuite = unittest.TestLoader().discover('.')
        unittest.TextTestRunner(verbosity=1).run(testsuite)
    elif mode == 4:
        run_server(True)
    elif mode == 5:
        run_standalone(True)

    """
        wp = WorkerPool()
        worker = StandaloneConnection()
        worker.connectWorker("","","")
        wp.addWorker(worker)
    
        jreq = JobRequest()
        jreq.seedUrls = ["https://example.com"]
        jreq.requestedDepth = 1
    
        jr = JobRunner(jreq, wp)
        print(jr.run())
    """
