import unittest
from unittest.mock import Mock, call

from server.standaloneConnection import StandaloneConnection


class Test_StandaloneConnection(unittest.TestCase):
    def test_1(self):
        request_method = Mock()
        request_method.return_value = "<p>Content of website that contains single email: osoba@example.com and two ip " \
                                      "addresses: 10.0.0.1 and 20.1.0.3 </p>"

        request_int = Mock()
        request_int.getWebsiteContent = request_method

        sc = StandaloneConnection(request_int)
        sc.connectWorker("", "", "")
        sc.worker.registerAllDefaultPlugins()

        sc.sendBatch(["abc"], 0, ["IPv4"])

        wr = sc.receiveResponse()

        request_method.assert_called_once_with("abc")
        self.assertDictEqual(wr.discovered_tokens, {"IPv4": {"10.0.0.1", "20.1.0.3"}})
        self.assertEqual(wr.discovered_urls, [])
        self.assertEqual(wr.performed_requests, 1)

    def test_2(self):
        request_method = Mock()
        request_method.side_effect = ["<p>Content of website that contains single email: osoba@example.com and two ip addresses: 10.0.0.1 and 20.1.0.3 http://a.b</p>", "<p>http://c.d</p>"]

        request_int = Mock()
        request_int.getWebsiteContent = request_method

        sc = StandaloneConnection(request_int)
        sc.connectWorker("", "", "")
        sc.worker.registerAllDefaultPlugins()

        sc.sendBatch(["1", "2"], 0, ["IPv4"])

        wr = sc.receiveResponse()

        calls = [call("1"), call("2")]
        request_method.assert_has_calls(calls)
        self.assertDictEqual(wr.discovered_tokens, {"IPv4": {"10.0.0.1", "20.1.0.3"}})
        self.assertListEqual(wr.discovered_urls, ["http://a.b", "http://c.d"])
        self.assertSetEqual(wr.website_map.getArcs(), {("1", "http://a.b"), ("2", "http://c.d")})
        self.assertEqual(wr.performed_requests, 2)

