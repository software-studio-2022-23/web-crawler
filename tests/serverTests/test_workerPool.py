import unittest

import server.workerPool
import tests
import tests.utils.workerConnectionS


class Test_workerPool(unittest.TestCase):
    def test_addWorker_1(self):
        wp = server.workerPool.WorkerPool()
        w = tests.utils.workerConnectionS.WorkerConnectionS()

        self.assertEqual(w.numberOfAliveChecks, 0, "Alive check before adding")
        self.assertEqual(wp.getNumWorkers(), 0, "Workers present before test")
        wp.addWorker(w)
        self.assertEqual(w.numberOfAliveChecks, 1, "Error with checking if connection is alive before adding")
        self.assertEqual(wp.getNumWorkers(), 1, "Incorrect num of workers")
        pass

    def test_batches(self):
        wp = server.workerPool.WorkerPool()
        urls = ["A", "B", "C", "D"]

        self.assertEqual(wp.prepareBatches(urls, 2), [["A", "B"], ["C", "D"]], "Incorrect batch division")
        self.assertEqual(wp.prepareBatches(urls, 3), [["A"], ["B"], ["C", "D"]], "Incorrect batch division")
        pass

    def test_reserveWorkers(self):
        wp = server.workerPool.WorkerPool()
        w1 = tests.utils.workerConnectionS.WorkerConnectionS(idConn=1)
        w2 = tests.utils.workerConnectionS.WorkerConnectionS(idConn=2)
        wp.addWorker(w1)
        wp.addWorker(w2)
        self.assertEqual(wp.getNumWorkers(), 2, "Adding workers failed")

        reserved = wp.reserveWorkers(1)
        self.assertEqual(len(reserved), 1, "Incorrect number of workers")
        self.assertEqual(reserved[0].getConnectionIdentifier(), 1, "Incorrect worker")
        self.assertEqual(reserved[0], w1, "Incorrect instance")
        self.assertEqual(w1.reservation, True, "Reservation not placed")
        pass

    def test_reserveWorkers_inf(self):
        wp = server.workerPool.WorkerPool()
        w1 = tests.utils.workerConnectionS.WorkerConnectionS(idConn=1)
        w2 = tests.utils.workerConnectionS.WorkerConnectionS(idConn=2)
        wp.addWorker(w1)
        wp.addWorker(w2)
        self.assertEqual(wp.getNumWorkers(), 2, "Adding workers failed")

        reserved = wp.reserveWorkers(-1)
        self.assertEqual(len(reserved), 2, "Incorrect number of workers")
        self.assertEqual(w1.reservation, True, "Reservation not placed")
        self.assertEqual(w2.reservation, True, "Reservation not placed")
        pass

    def test_sendForProcessing_1(self):
        wp = server.workerPool.WorkerPool()
        w1 = tests.utils.workerConnectionS.WorkerConnectionS(idConn=1)
        w2 = tests.utils.workerConnectionS.WorkerConnectionS(idConn=2)
        wp.addWorker(w1)
        wp.addWorker(w2)
        self.assertEqual(wp.getNumWorkers(), 2, "Adding workers failed")
        urls = ["A", "B", "C", "D"]

        resp = wp.sendUrlsForProcessing(urls, 1)
        self.assertEqual(w1.reservation, False)
        self.assertEqual(w1.sentBatch, urls)
        self.assertEqual(w2.reservation, False)
        self.assertEqual(w2.sentBatch, None)

        pass
