import unittest

from server.jobResult import JobResult
from utils.websiteMapGraph import WebsiteMapGraph
from utils.wordPhrasesVector import WordPhrasesVector
from utils.workerResponse import WorkerResponse


class Test_JobResult(unittest.TestCase):

    def test_1(self):
        jr = JobResult()

        self.assertEqual(jr.number_of_requests, 0)
        self.assertSetEqual(jr.website_map.getArcs(), set())
        self.assertDictEqual(jr.tokens, {})
        self.assertListEqual(jr.word_phrases.getWords(), [{}, {}, {}, {}])

    def test_2(self):
        jr = JobResult()

        self.assertEqual(jr.number_of_requests, 0)
        self.assertSetEqual(jr.website_map.getArcs(), set())
        self.assertDictEqual(jr.tokens, {})
        self.assertListEqual(jr.word_phrases.getWords(), [{}, {}, {}, {}])

        wr = WorkerResponse()

        wp = WordPhrasesVector(4)

        wr.performed_requests = 3
        wr.website_map = WebsiteMapGraph()
        wr.website_map.addNewArc("a","b")
        wr.discovered_tokens = {"tokentype": {"a", "b"}}
        wr.discovered_urls = ["h", "f"]
        wr.wordPhrasesList = wp

        jr.merge_with_worker_response(wr)

        self.assertEqual(jr.number_of_requests, 3)
        self.assertSetEqual(jr.website_map.getArcs(), {("a", "b")})
        self.assertDictEqual(jr.tokens, {"tokentype": {"a", "b"}})
        self.assertEqual(jr.word_phrases.getWords(), wp.getWords())

