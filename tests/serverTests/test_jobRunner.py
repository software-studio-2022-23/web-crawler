import unittest
from unittest.mock import Mock

from server.jobRunner import JobRunner
from server.standaloneConnection import StandaloneConnection
from server.workerPool import WorkerPool


class Test_JobRunner(unittest.TestCase):

    def test_1(self):
        request_method = Mock()
        # dane ma czytać z plików i w tym drugim też
        request_method.return_value = "<p>Content of website that contains single email: osoba@example.com and two ip " \
                                      "addresses: 10.0.0.1 and 20.1.0.3 </p> "
        request_int = Mock()
        request_int.getWebsiteContent = request_method

        sc = StandaloneConnection(request_int)
        sc.connectWorker("", "", "")
        sc.worker.registerAllDefaultPlugins()

        wp = WorkerPool()
        wp.addWorker(sc)

        # runner = JobRunner()