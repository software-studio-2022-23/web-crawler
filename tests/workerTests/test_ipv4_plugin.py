import unittest

from worker.extractor_plugins.ipv4 import IPV4_plugin


class Test_IPV4Plubin(unittest.TestCase):
    def test_1(self):
        pl = IPV4_plugin()
        r = pl.extract("192.168.1.1")
        self.assertEqual(r, {"192.168.1.1"})


    def test_2(self):
        pl = IPV4_plugin()
        r = pl.extract("192.168.1.1 127.0.0.1       engo3n5ogin3g3pm 84.90.20.1")
        self.assertEqual(r, {"192.168.1.1", "127.0.0.1", "84.90.20.1"})
        