import unittest

from worker.extractor_plugins.charsets import END_PUNCTUATION, SEPARATOR_DEFANGS, URL_SPLIT_STR


class Test_charset(unittest.TestCase):
    def test_1(self):
        self.assertEqual(END_PUNCTUATION, r"[\.\?>\"'\)!,}:;\u201d\u2019\uff1e\uff1c\]]*")
        self.assertEqual(SEPARATOR_DEFANGS,  r"[\(\)\[\]{}<>\\]")
        self.assertEqual(URL_SPLIT_STR,  r"[>\"'\),};]")
