import unittest

from worker.extractor_plugins.email import EMAIL_plugin


class Test_EmailPlugin(unittest.TestCase):

    def test_1(self):
        pl = EMAIL_plugin()
        r = pl.extract("osoba@example.com")
        self.assertEqual(r, {"osoba@example.com"})

    def test_2(self):
        pl = EMAIL_plugin()
        r = pl.extract("osoba@example.com,")
        self.assertEqual(r, {"osoba@example.com"})

    def test_3(self):
        pl = EMAIL_plugin()
        r = pl.extract("osoba@example.com neiogeorgn3[4gt34g34g abc@cd.pl")
        self.assertEqual(r, {"osoba@example.com", "abc@cd.pl"})
        