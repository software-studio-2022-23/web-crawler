import unittest
from unittest.mock import Mock

from worker.extractor_plugins.ipv4 import IPV4_plugin
from worker.responseProcessor import ResponseProcessor


class Test_ResponseProcessor(unittest.TestCase):
    def test_1(self):
        mockPlugin = Mock()

        rp = ResponseProcessor()
        rp.registerPlugin("testToken", mockPlugin)

        result = rp.process(["testToken"], "abc")

        method: Mock = mockPlugin.extract
        method.assert_called_once_with("abc")

    def test_2(self):
        mockPlugin = Mock()

        rp = ResponseProcessor()
        rp.registerPlugin("testToken", mockPlugin)

        result = rp.process(["otherToken"], "abc")

        method: Mock = mockPlugin.extract
        method.assert_not_called()

    def test_3(self):
        mockPlugin1 = Mock()
        mockPlugin2 = Mock()

        rp = ResponseProcessor()
        rp.registerPlugin("tokenA", mockPlugin1)
        rp.registerPlugin("tokenB", mockPlugin2)

        result = rp.process(["tokenA", "tokenB"], "abc")

        method1: Mock = mockPlugin1.extract
        method2: Mock = mockPlugin2.extract

        method1.assert_called_once_with("abc")
        method2.assert_called_once_with("abc")

    def test_4(self):
        mockPlugin1 = Mock()
        mockPlugin2 = Mock()

        rp = ResponseProcessor()
        rp.registerPlugin("tokenA", mockPlugin1)
        rp.registerPlugin("tokenB", mockPlugin2)

        result = rp.process(["tokenA", "tokenB", "notAToken"], "abc")

        method1: Mock = mockPlugin1.extract
        method2: Mock = mockPlugin2.extract

        method1.assert_called_once_with("abc")
        method2.assert_called_once_with("abc")

    def test_5(self):
        mockPlugin1 = Mock()
        mockPlugin2 = Mock()

        rp = ResponseProcessor()
        rp.registerPlugin("tokenA", mockPlugin1)
        rp.registerPlugin("tokenB", mockPlugin2)

        result = rp.process(["tokenA"], "abc")

        method1: Mock = mockPlugin1.extract
        method2: Mock = mockPlugin2.extract

        method1.assert_called_once_with("abc")
        method2.assert_not_called()

    # -------------------------------------

    def test_return_1(self):
        extract_method: Mock = Mock()
        extract_method.return_value = {"foundToken1", "foundToken2"}

        mock_plugin = Mock()
        mock_plugin.extract = extract_method

        rp = ResponseProcessor()
        rp.registerPlugin("A", mock_plugin)

        result = rp.process(["A"], "abc")

        extract_method.assert_called_once_with("abc")
        self.assertDictEqual(result, {"A": {"foundToken1", "foundToken2"}})

    def test_with_ipv4plugin_1(self):
        rp = ResponseProcessor()
        ipv4pl = IPV4_plugin()
        rp.registerPlugin("ip", ipv4pl)

        result = rp.process(["ip"], "abc 123 192.168.1.100")

        self.assertDictEqual(result, {"ip": {"192.168.1.100"}})

    def test_with_ipv4plugin_2(self):
        rp = ResponseProcessor()
        ipv4pl = IPV4_plugin()
        rp.registerPlugin("ip", ipv4pl)

        result = rp.process(["ip"], "abc 123")

        self.assertDictEqual(result, {"ip":set()})

    def test_with_ipv4plugin_3(self):
        rp = ResponseProcessor()
        ipv4pl = IPV4_plugin()
        rp.registerPlugin("ip", ipv4pl)

        result = rp.process(["ip"], "abc 123 192.168.1.100 10.2.3.1")

        self.assertDictEqual(result, {"ip": {"192.168.1.100", "10.2.3.1"}})

    def test_with_ipv4plugin_4(self):
        rp = ResponseProcessor()
        ipv4pl = IPV4_plugin()
        rp.registerPlugin("ip", ipv4pl)

        result = rp.process([], "abc 123 192.168.1.100 10.2.3.1")

        self.assertDictEqual(result, {})

    def test_with_ipv4plugin_5(self):
        rp = ResponseProcessor()

        result = rp.process(["ip"], "abc 123 192.168.1.100 10.2.3.1")

        self.assertDictEqual(result, {})

    def test_with_ipv4plugin_7(self):
        rp = ResponseProcessor()
        ipv4pl = IPV4_plugin()
        rp.registerPlugin("ip", ipv4pl)

        result = rp.process(["ip"], "abc 123 0.0.0.0")

        self.assertDictEqual(result, {"ip": {"0.0.0.0"}})
