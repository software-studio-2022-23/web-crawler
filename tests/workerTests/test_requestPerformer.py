import time
import unittest
from unittest.mock import Mock

from worker.requestPerformer import RequestPerformer


class Test_RequestPerformer(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        with open('tests/utils/example.com.txt', 'r') as file:
            data = file.read()
            cls.example_com = data

    def test_0(self):
        rp = RequestPerformer()
        cnt, links = rp.getContent("https://example.com/")
        self.assertEqual(
            "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in illustrative examples in documents. You may use this\n    domain in literature without prior coordination or asking for permission.\nMore information...\n\n\n\n",
            cnt)
        self.assertEqual(links, ['https://www.iana.org/domains/example'])

    def test_1(self):
        request_int_method = Mock()
        request_int = Mock()
        request_int_method.return_value = self.example_com
        request_int.getWebsiteContent = request_int_method

        rp = RequestPerformer(request_int)
        cnt, links = rp.getContent("https://example.com/")
        self.assertEqual(
            "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in illustrative examples in documents. You may use this\n    domain in literature without prior coordination or asking for permission.\nMore information...\n\n\n\n",
            cnt)
        self.assertEqual(links, ['https://www.iana.org/domains/example'])

    def test_2(self):
        request_int_method = Mock()
        request_int = Mock()
        request_int_method.return_value = self.example_com
        request_int.getWebsiteContent = request_int_method

        rp = RequestPerformer(request_int)
        rp.setDelay(1)
        start_time = time.time()
        cnt1, links1 = rp.getContent("https://example.com/")
        cnt2, links2 = rp.getContent("https://example.com/")
        end_time = time.time()

        self.assertEqual(cnt1, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links1, ['https://www.iana.org/domains/example'])

        self.assertEqual(cnt2, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links2, ['https://www.iana.org/domains/example'])

        self.assertGreaterEqual(end_time - start_time, 1)

    def test_3(self):
        request_int_method = Mock()
        request_int = Mock()
        request_int_method.return_value = self.example_com
        request_int.getWebsiteContent = request_int_method

        rp = RequestPerformer(request_int)
        rp.setDelay(10)
        start_time = time.time()
        cnt1, links1 = rp.getContent("https://example.com/")
        cnt2, links2 = rp.getContent("https://example.com/")
        end_time = time.time()

        self.assertEqual(cnt1, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links1, ['https://www.iana.org/domains/example'])

        self.assertEqual(cnt2, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links2, ['https://www.iana.org/domains/example'])

        self.assertGreaterEqual(end_time - start_time, 10)

    def test_4(self):
        request_int_method = Mock()
        request_int = Mock()
        request_int_method.return_value = self.example_com
        request_int.getWebsiteContent = request_int_method

        rp = RequestPerformer(request_int)
        rp.setDelay(10)
        start_time = time.time()
        cnt1, links1 = rp.getContent("https://example.com/")
        end_time = time.time()

        self.assertEqual(cnt1, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links1, ['https://www.iana.org/domains/example'])

        self.assertLess(end_time - start_time, 10)

    def test_5(self):
        request_int_method = Mock()
        request_int = Mock()
        request_int_method.return_value = self.example_com
        request_int.getWebsiteContent = request_int_method

        rp = RequestPerformer(request_int)
        rp.setDelay(10)
        start_time = time.time()
        cnt1, links1 = rp.getContent("https://example.com/")
        cnt2, links2 = rp.getContent("https://example.com/")
        end_time = time.time()

        self.assertEqual(cnt1, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links1, ['https://www.iana.org/domains/example'])

        self.assertEqual(cnt2, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links2, ['https://www.iana.org/domains/example'])

        self.assertGreaterEqual(end_time - start_time, 10)

        rp.setDelay(1)
        start_time = time.time()
        cnt1, links1 = rp.getContent("https://example.com/")
        cnt2, links2 = rp.getContent("https://example.com/")
        end_time = time.time()

        self.assertEqual(cnt1, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links1, ['https://www.iana.org/domains/example'])

        self.assertEqual(cnt2, "\n\n\nExample Domain\n\n\n\n\n\n\n\nExample Domain\nThis domain is for use in "
                               "illustrative examples in documents. You may use this\n    domain in literature "
                               "without prior coordination or asking for permission.\nMore information...\n\n\n\n")
        self.assertEqual(links2, ['https://www.iana.org/domains/example'])

        self.assertGreaterEqual(end_time - start_time, 1)
        self.assertLessEqual(end_time - start_time, 10)


# todo change to mock objects