import time
import unittest
from unittest.mock import Mock, call

from worker.extractor_plugins.email import EMAIL_plugin
from worker.extractor_plugins.ipv4 import IPV4_plugin
from worker.workerMainRoutine import WorkerMainRoutine


class test_WorkerMainRoutine(unittest.TestCase):

    def test_1(self):
        request_int = Mock()
        request_method = Mock()

        request_method.return_value = "<p>Content of website that contains single email: osoba@example.com and two ip " \
                                      "addresses: 10.0.0.1 and 20.1.0.3 </p>"

        request_int.getWebsiteContent = request_method

        wmr = WorkerMainRoutine(request_int)
        ip4 = IPV4_plugin()
        email = EMAIL_plugin()
        wmr.registerPluginForToken("ip", ip4)
        wmr.registerPluginForToken("email", email)

        result = wmr.processSingleUrl("abc", 1)

        request_method.assert_called_once_with("abc")
        self.assertDictEqual(result.discovered_tokens, {"ip": {"10.0.0.1", "20.1.0.3"}, "email": {"osoba@example.com"}})
        self.assertEqual(result.performed_requests, 1)
        self.assertSetEqual(result.website_map.getArcs(), set())


    def test_2(self):
        request_int = Mock()
        request_method = Mock()

        request_method.return_value = "<p>Content of website that contains single email: osoba@example.com and two ip " \
                                      "addresses: 10.0.0.1 and 20.1.0.3 </p>"

        request_int.getWebsiteContent = request_method

        wmr = WorkerMainRoutine(request_int)
        ip4 = IPV4_plugin()
        email = EMAIL_plugin()
        wmr.registerPluginForToken("ip", ip4)
        wmr.registerPluginForToken("email", email)

        result = wmr.processBatch(["abc"],1)

        request_method.assert_called_once_with("abc")
        self.assertDictEqual(result.discovered_tokens, {"ip": {"10.0.0.1", "20.1.0.3"}, "email": {"osoba@example.com"}})
        self.assertEqual(result.performed_requests, 1)
        self.assertSetEqual(result.website_map.getArcs(), set())

    def test_3(self):
        request_int = Mock()
        request_method = Mock()

        request_method.return_value = "<p>Content of website that contains single email: osoba@example.com and two ip " \
                                      "addresses: 10.0.0.1 and 20.1.0.3 </p>"

        request_int.getWebsiteContent = request_method

        wmr = WorkerMainRoutine(request_int)
        ip4 = IPV4_plugin()
        email = EMAIL_plugin()
        wmr.registerPluginForToken("ip", ip4)
        wmr.registerPluginForToken("email", email)

        result = wmr.processBatch(["abc", "abc"], 1)

        request_method.assert_called_with("abc")
        # print(result.discovered_tokens)
        self.assertDictEqual(result.discovered_tokens, {"ip": {"10.0.0.1", "20.1.0.3"}, "email": {"osoba@example.com"}})
        self.assertEqual(result.performed_requests, 2)
        self.assertSetEqual(result.website_map.getArcs(), set())


    def test_4(self):
        request_int = Mock()
        request_method = Mock()

        request_method.side_effect = ["<p>Content of website that contains single email: osoba@example.com and two ip "
                                      "addresses: 10.0.0.1 and 20.1.0.3 http://abc.c</p>", "<p>another website "
                                                                                           "10.1.1.1</p>"]

        request_int.getWebsiteContent = request_method

        wmr = WorkerMainRoutine(request_int)
        ip4 = IPV4_plugin()
        email = EMAIL_plugin()
        wmr.registerPluginForToken("ip", ip4)
        wmr.registerPluginForToken("email", email)

        result = wmr.processBatch(["abc"], 1)

        request_method.assert_called_once_with("abc")
        # print(result.discovered_tokens)
        self.assertDictEqual(result.discovered_tokens, {"ip": {"10.0.0.1", "20.1.0.3"}, "email": {"osoba@example.com"}})
        self.assertEqual(result.performed_requests, 1)
        self.assertSetEqual(result.website_map.getArcs(), {("abc", "http://abc.c")})


    def test_5(self):
        request_int = Mock()
        request_method = Mock()

        request_method.side_effect = ["<p>Content of website that contains single email: osoba@example.com and two ip "
                                      "addresses: 10.0.0.1 and 20.1.0.3 http://abc.c</p>", "<p>another website "
                                                                                           "10.1.1.1</p>"]

        request_int.getWebsiteContent = request_method

        wmr = WorkerMainRoutine(request_int)
        ip4 = IPV4_plugin()
        email = EMAIL_plugin()
        wmr.registerPluginForToken("ip", ip4)
        wmr.registerPluginForToken("email", email)

        result = wmr.processBatch(["abc", "bcd"], 1)

        calls = [call("abc"), call("bcd")]
        request_method.assert_has_calls(calls)
        self.assertDictEqual(result.discovered_tokens, {"ip": {"10.0.0.1", "20.1.0.3", "10.1.1.1"}, "email": {"osoba@example.com"}})
        self.assertEqual(result.performed_requests, 2)
        self.assertSetEqual(result.website_map.getArcs(), {("abc", "http://abc.c")})

    def test_6(self):
        request_int = Mock()
        request_method = Mock()

        request_method.side_effect = ["<p>Content of website that contains single email: osoba@example.com and two ip "
                                      "addresses: 10.0.0.1 and 20.1.0.3 http://abc.c</p>", "<p>another website "
                                                                                           "10.1.1.1</p>"]

        request_int.getWebsiteContent = request_method

        wmr = WorkerMainRoutine(request_int)
        ip4 = IPV4_plugin()
        email = EMAIL_plugin()
        wmr.registerPluginForToken("ip", ip4)
        wmr.registerPluginForToken("email", email)

        result = wmr.processBatch(["abc", "bcd"], 0, ["ip"])

        calls = [call("abc"), call("bcd")]
        request_method.assert_has_calls(calls)
        self.assertDictEqual(result.discovered_tokens, {"ip": {"10.0.0.1", "20.1.0.3", "10.1.1.1"}})
        self.assertEqual(result.performed_requests, 2)
        self.assertSetEqual(result.website_map.getArcs(), {("abc", "http://abc.c")})


    def test_7(self):
        request_int = Mock()
        request_method = Mock()

        request_method.side_effect = ["<p>Content of website that contains single email: osoba@example.com and two ip "
                                      "addresses: 10.0.0.1 and 20.1.0.3 http://abc.c</p>", "<p>another website "
                                                                                           "10.1.1.1</p>"]

        request_int.getWebsiteContent = request_method

        wmr = WorkerMainRoutine(request_int)
        ip4 = IPV4_plugin()
        email = EMAIL_plugin()
        wmr.registerPluginForToken("ip", ip4)
        wmr.registerPluginForToken("email", email)

        start_time = time.time()
        result = wmr.processBatch(["abc", "bcd"], 10, ["ip"])
        end_time = time.time()

        calls = [call("abc"), call("bcd")]
        request_method.assert_has_calls(calls)
        self.assertDictEqual(result.discovered_tokens, {"ip": {"10.0.0.1", "20.1.0.3", "10.1.1.1"}})
        self.assertEqual(result.performed_requests, 2)
        self.assertSetEqual(result.website_map.getArcs(), {("abc", "http://abc.c")})
        self.assertGreaterEqual(end_time-start_time, 10)
