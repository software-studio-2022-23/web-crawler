import unittest

from utils.wordPhrasesVector import WordPhrasesVector


class Test_WordPhrasesVector(unittest.TestCase):
    def test_length_2(self):
        wpv = WordPhrasesVector(2)
        self.assertEqual(wpv.max_len, 2)
        self.assertEqual(len(wpv.word_phrases), 2)
        self.assertEqual(wpv.word_phrases[0].getNumberOfWords(), 1)
        self.assertEqual(wpv.word_phrases[1].getNumberOfWords(), 2)

    def test_length_3(self):
        wpv = WordPhrasesVector(3)
        self.assertEqual(wpv.max_len, 3)
        self.assertEqual(len(wpv.word_phrases), 3)
        self.assertEqual(wpv.word_phrases[0].getNumberOfWords(), 1)
        self.assertEqual(wpv.word_phrases[1].getNumberOfWords(), 2)
        self.assertEqual(wpv.word_phrases[2].getNumberOfWords(), 3)

    def test_merging_1(self):
        wpv1 = WordPhrasesVector(1)
        wpv2 = WordPhrasesVector(1)

        wpv1.word_phrases[0].consumeString("A A A")
        wpv2.word_phrases[0].consumeString("A B C")

        wpv1.merge(wpv2)

        self.assertEqual(wpv1.word_phrases[0].getWords(), {"A": 4, "B": 1, "C": 1})

    def test_merging_2(self):
        wpv1 = WordPhrasesVector(2)
        wpv2 = WordPhrasesVector(2)

        wpv1.word_phrases[0].consumeString("A A A")
        wpv1.word_phrases[1].consumeString("A A A")
        wpv2.word_phrases[0].consumeString("A B C")
        wpv2.word_phrases[1].consumeString("A B C")

        wpv1.merge(wpv2)

        self.assertEqual(wpv1.word_phrases[0].getWords(), {"A": 4, "B": 1, "C": 1})
        self.assertEqual(wpv1.word_phrases[1].getWords(), {"A A": 2, "A B": 1, "B C": 1})

    def test_merging_3(self):
        wpv1 = WordPhrasesVector(2)
        wpv2 = WordPhrasesVector(2)

        wpv1.consumeString("A A A")
        wpv2.consumeString("A B C")

        wpv1.merge(wpv2)

        self.assertEqual(wpv1.word_phrases[0].getWords(), {"A": 4, "B": 1, "C": 1})
        self.assertEqual(wpv1.word_phrases[1].getWords(), {"A A": 2, "A B": 1, "B C": 1})

    def test_getWords(self):
        wpv1 = WordPhrasesVector(2)
        wpv2 = WordPhrasesVector(2)

        wpv1.consumeString("A A A")
        wpv2.consumeString("A B C")

        wpv1.merge(wpv2)

        self.assertEqual(wpv1.getWords(),[{"A": 4, "B": 1, "C": 1}, {"A A": 2, "A B": 1, "B C": 1}])
