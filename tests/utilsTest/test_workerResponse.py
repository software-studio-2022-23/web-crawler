import unittest

import utils.workerResponse
import utils.wordPhrases


class Test_workerResponse(unittest.TestCase):
    def test_merge(self):
        wr1 = utils.workerResponse.WorkerResponse()
        # wr1.discovered_image_sources = [2]
        # wr1.discovered_phone_numbers = [3]
        wr1.discovered_urls = [4]
        # wr1.discovered_scripts = [5]
        # wr1.discovered_emails = [6]
        # wr1.discovered_credit_cards = [33]
        # wr1.discovered_passwords = [56]
        wr1.performed_requests = 5
        wr1.discovered_tokens = {1: set([1, 2, 3])}

        wr2 = utils.workerResponse.WorkerResponse()
        # wr2.discovered_image_sources = [8]
        # wr2.discovered_phone_numbers = [9]
        wr2.discovered_urls = [10]
        # wr2.discovered_scripts = [11]
        # wr2.discovered_emails = [12]
        # wr2.discovered_credit_cards = [44]
        # wr2.discovered_passwords = [27]
        wr2.performed_requests = 2
        wr2.discovered_tokens = {1: set([5, 4, 3]), 2: set([1])}

        wr1.merge(wr2)

        # self.assertEqual(wr1.discovered_image_sources, [2, 8], "Incorrect merging")
        # self.assertEqual(wr1.discovered_phone_numbers, [3, 9], "Incorrect merging")
        self.assertEqual(wr1.discovered_urls, [4, 10], "Incorrect merging")
        # self.assertEqual(wr1.discovered_scripts, [5, 11], "Incorrect merging")
        # self.assertEqual(wr1.discovered_emails, [6, 12], "Incorrect merging")
        # self.assertEqual(wr1.discovered_credit_cards, [33, 44], "Incorrect merging")
        # self.assertEqual(wr1.discovered_passwords, [56, 27], "Incorrect merging")
        self.assertEqual(wr1.performed_requests, 7, "Incorrect merging")
        self.assertEqual(wr1.discovered_tokens, {1: set([1, 2, 3, 4, 5]), 2: set([1])})

    def test_merge_wordPhrases_1(self):
        wr1 = utils.workerResponse.WorkerResponse()
        wr2 = utils.workerResponse.WorkerResponse()

        wp1 = utils.wordPhrases.WordPhrases(1)
        wp1.consumeString("A A B B B")

        wp2 = utils.wordPhrases.WordPhrases(1)
        wp2.consumeString("A B B")

        wr1.wordPhrasesList.word_phrases[1] = wp1
        wr2.wordPhrasesList.word_phrases[1] = wp2

        wr1.merge(wr2)

        self.assertEqual(wr1.wordPhrasesList.word_phrases[1].getWords(), {"A": 3, "B": 5})
