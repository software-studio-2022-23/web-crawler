import unittest

import utils.wordPhrases


class Test_WordPhrases(unittest.TestCase):
    def test_1_word_1(self):
        wp = utils.wordPhrases.WordPhrases(1)
        wp.consumeString("A")

        self.assertEqual(wp.getWords(), {"A": 1})

    def test_1_word_2(self):
        wp = utils.wordPhrases.WordPhrases(1)
        wp.consumeString("A B B A")

        self.assertEqual(wp.getWords(), {"A": 2, "B": 2})

    def test_1_word_3(self):
        wp = utils.wordPhrases.WordPhrases(1)
        wp.consumeString("Once there was a dog. The dog was a happy dog!")

        self.assertEqual(wp.getWords(), {"Once": 1,
                                         "there": 1,
                                         "was": 2,
                                         "a": 2,
                                         "dog": 3,
                                         "The": 1,
                                         "happy": 1
                                         })

    def test_2_word_1(self):
        wp = utils.wordPhrases.WordPhrases(2)
        wp.consumeString("Once there was a dog. The dog was a happy dog!")

        self.assertEqual(wp.getWords(), {"Once there": 1,
                                         "there was": 1,
                                         "was a": 2,
                                         "a dog": 1,
                                         "dog The": 1,
                                         "The dog": 1,
                                         "dog was": 1,
                                         "a happy": 1,
                                         "happy dog": 1
                                         }, "Incorrect merging")
    def test_merging_1(self):
        wp1 = utils.wordPhrases.WordPhrases(1)
        wp2 = utils.wordPhrases.WordPhrases(1)

        wp1.consumeString("A A A")
        wp2.consumeString("A A")

        wp1.merge(wp2)

        self.assertEqual(wp1.getWords(), {"A": 5})

    def test_merging_2(self):
        wp1 = utils.wordPhrases.WordPhrases(1)
        wp2 = utils.wordPhrases.WordPhrases(1)

        wp1.consumeString("A A A B")
        wp2.consumeString("A A")

        wp1.merge(wp2)

        self.assertEqual(wp1.getWords(), {"A": 5, "B": 1})

    def test_merging_3(self):
        wp1 = utils.wordPhrases.WordPhrases(1)
        wp2 = utils.wordPhrases.WordPhrases(1)

        wp1.consumeString("A A A")
        wp2.consumeString("A A B")

        wp1.merge(wp2)

        self.assertEqual(wp1.getWords(), {"A": 5, "B": 1})
