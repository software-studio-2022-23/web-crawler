import unittest

from utils.websiteMapGraph import WebsiteMapGraph


class Test_WebsiteMapGraph(unittest.TestCase):
    def test_simple(self):
        wmg = WebsiteMapGraph()
        wmg.addNewArc("A", "B")
        wmg.addNewArc("A", "B")
        wmg.addNewArc("B", "C")

        self.assertEqual(wmg.getArcs(), set([("A", "B"), ("B", "C")]))



    def test_merge(self):
        wmg1 = WebsiteMapGraph()
        wmg1.addNewArc("A", "B")
        wmg1.addNewArc("A", "B")
        wmg1.addNewArc("B", "C")

        wmg2 = WebsiteMapGraph()
        wmg2.addNewArc("A", "B")
        wmg2.addNewArc("A", "C")

        wmg1.merge(wmg2)

        self.assertEqual(wmg1.getArcs(), set([("A", "B"), ("B", "C"), ("A", "C")]))
