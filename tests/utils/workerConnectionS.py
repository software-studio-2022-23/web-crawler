from typing import List

import server.workerConnection
from utils.workerResponse import WorkerResponse


class WorkerConnectionS(server.workerConnection.WorkerConnection):
    def releaseReservation(self) -> bool:
        self.reservation = False
        return True

    def __init__(self,
                 alive: bool = True,
                 idConn: int = 0,
                 reservation: bool = False,
                 resp: WorkerResponse = WorkerResponse()
                 ):
        self.alive = alive
        self.id = idConn
        self.reservation = reservation
        self.resp = resp
        self.sentBatch = None
        self.numberOfAliveChecks = 0

    def sendBatch(self, batch: list, delay: int = 1, tokens: List[str] = None):
        self.sentBatch = batch
        pass

    def receiveResponse(self) -> WorkerResponse:
        if self.sentBatch is not None:
            return self.resp
        return None

    def pauseWorker(self):
        pass

    def reasumeWorker(self):
        pass

    def connectWorker(self, address, port, password):
        pass

    def getConnectionIdentifier(self) -> int:
        return self.id

    def isAlive(self) -> bool:
        self.numberOfAliveChecks += 1
        return self.alive

    def placeReservaion(self) -> bool:
        if not self.reservation:
            self.reservation = True
            return True
        return False
