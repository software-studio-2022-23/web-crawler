# from typing import Annotated, List
import copy
from collections import defaultdict
from typing import List, Dict, Set

from utils.websiteMapGraph import WebsiteMapGraph
from utils.wordPhrasesVector import WordPhrasesVector


# Annotated requires python >= 3.9 and it requires windows > 7


class WorkerResponse:
    wordPhrasesList: WordPhrasesVector
    discovered_urls: List[str]
    # discovered_emails: List[str]
    # discovered_scripts: List[str]
    # discovered_phone_numbers: List[str]
    # discovered_image_sources: List[str]
    # discovered_credit_cards: List[str]
    # discovered_passwords: List[str]
    website_map: WebsiteMapGraph
    performed_requests: int

    discovered_tokens: Dict[str, Set[str]]  # name of token; set of discovered tokens

    def __init__(self):
        self.discovered_urls = []
        # self.discovered_emails = []
        # self.discovered_scripts = []
        # self.discovered_phone_numbers = []
        # self.discovered_image_sources = []
        # self.discovered_credit_cards = []
        # self.discovered_passwords = []
        self.website_map = WebsiteMapGraph()
        self.performed_requests = 0
        # self.discovered_image_alt_desc = []
        # self.discovered_
        # self.wordPhrasesList: Annotated[List[WordPhrases], 'Word phrases list'] = []
        self.wordPhrasesList = WordPhrasesVector(4)

        self.discovered_tokens = defaultdict(lambda: set())

    def __str__(self):
        result = "------- { WORKER RESULT } ------- \n"
        result += "# Map: \n"
        result += str(self.website_map.getArcs())
        result += "\n# Word phrases: \n"
        result += str([dict(d) for d in self.wordPhrasesList.getWords()])
        result += "\n# Tokens: \n"
        result += str(dict(self.discovered_tokens))
        result += "\n------- { ############# } ------- \n"
        return result

    def merge(self, other_response: 'WorkerResponse') -> 'WorkerResponse':
        # merges with self, returns self
        self.discovered_urls += other_response.discovered_urls
        # self.discovered_emails += other_response.discovered_emails
        # self.discovered_scripts += other_response.discovered_scripts
        # self.discovered_phone_numbers += other_response.discovered_phone_numbers
        # self.discovered_image_sources += other_response.discovered_image_sources
        # self.discovered_credit_cards += other_response.discovered_credit_cards
        # self.discovered_passwords += other_response.discovered_passwords
        self.performed_requests += other_response.performed_requests
        self.website_map.merge(other_response.website_map)
        self.wordPhrasesList.merge(other_response.wordPhrasesList)

        # self.discovered_tokens.update(other_response.discovered_tokens)
        for token_name in other_response.discovered_tokens.keys():
            if token_name in self.discovered_tokens.keys():
                self.discovered_tokens[token_name] = \
                    self.discovered_tokens[token_name].union(other_response.discovered_tokens[token_name])
            else:
                self.discovered_tokens[token_name] = set(copy.deepcopy(other_response.discovered_tokens[token_name]))
        return self
