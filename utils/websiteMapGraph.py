import copy
from typing import Tuple, Set


class WebsiteMapGraph:
    _list_of_arcs: Set[Tuple[str, str]]

    def __init__(self):
        self._list_of_arcs = set()

    def __copy__(self):
        wmg = WebsiteMapGraph()
        wmg._list_of_arcs = self._list_of_arcs.copy()
        return wmg

    def merge(self, other_graph: 'WebsiteMapGraph') -> 'WebsiteMapGraph':

        for arc in other_graph._list_of_arcs:
            if arc not in self._list_of_arcs:
                self._list_of_arcs.add(arc)

        return self

    def eguals(self, other: 'WebsiteMapGraph') -> bool:
        return self._list_of_arcs == other._list_of_arcs

    def addNewArc(self, origin_url: str, child_url: str):
        arc = (origin_url, child_url)
        if arc not in self._list_of_arcs:
            self._list_of_arcs.add((origin_url, child_url))

    def getArcs(self) -> Set[Tuple[str, str]]:
        return self._list_of_arcs.copy()

# Should contain graph representation of the scanned website
