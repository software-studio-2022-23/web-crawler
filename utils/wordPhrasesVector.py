from typing import List

from utils.wordPhrases import WordPhrases


class WordPhrasesVector:
    max_len: int
    word_phrases: List[WordPhrases]

    def __init__(self, n: int):
        self.max_len = n
        self.word_phrases = []
        for i in range(self.max_len):
            self.word_phrases += [WordPhrases(i+1)]

    def merge(self, other_vect: 'WordPhrasesVector') -> 'WordPhrasesVector':
        for i in range(min(self.max_len, other_vect.max_len)):  # Merges found word phrases
            self.word_phrases[i].merge(other_vect.word_phrases[i])
        return self

    def consumeString(self, s: str):
        for i in range(self.max_len):
            self.word_phrases[i].consumeString(s)

    def getWords(self):
        result = []
        for i in range(self.max_len):
            result.append(self.word_phrases[i].getWords())
        return result
