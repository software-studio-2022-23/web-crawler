from collections import defaultdict
import copy
import re

class WordPhrases:
    """Class responsible for creating word phrases counting array and updating it based on a given text"""
    _words: dict
    # Should allow customization of number of phrases

    # Dla serwera i wokera, po zakończeniu procesowania merge classses form worker and erver

    # statystyk pojedynczych słów to fraza długości 1

    def __init__(self, number_of_words: int = 1):
        self._numberOfWords = number_of_words
        self._words = defaultdict(lambda: 0)

    def getWords(self):
        return copy.deepcopy(self._words)

    def merge(self, other_wordphreses: 'WordPhrases') -> 'WordPhrases':
        self.mergeWords(other_wordphreses.getWords())
        return self

    def mergeWords(self, words: defaultdict):
        for word in words.keys():
            self._words[word] += words[word]

    def consumeString(self, string_to_process: str):
        string_to_process = string_to_process.replace("\n", " ")
        string_to_process = string_to_process.replace("\t", " ")
        string_to_process = string_to_process.replace("\r", " ")
        string_to_process = re.sub(' +', ' ', string_to_process)
        pattern = re.compile(r'[^a-zA-Z\d\s:ąęćęłóżźńś]+')    # [^a-zA-Z\d\s:] [\W_]
        stripped = pattern.sub('', string_to_process)

        list_of_single_words = stripped.split(" ")
        number_of_words = len(list_of_single_words)

        for i in range(number_of_words+1-self._numberOfWords):
            current_phrase = list_of_single_words[i:i+self._numberOfWords]
            current_phrase = " ".join(current_phrase)
            self._words[current_phrase] += 1

    def getNumberOfWords(self):
        return copy.deepcopy(self._numberOfWords)
