import threading

from typing import List

import server.workerConnection
import utils.workerResponse


class WorkerPool:
    """Class responsible for holding connections to all workers and managing them"""
    _paired_workers: List[server.workerConnection.WorkerConnection]

    def __init__(self):
        self._paired_workers = []

    def addWorker(self, conn: server.workerConnection.WorkerConnection):
        """Adds new connection to worker"""
        if conn.isAlive():
            self._paired_workers.append(conn)

    def removeWorker(self, conectionIdentifier: int):
        """Removes worker from the list, does not terminate connection"""
        # workerConnection.WorkerConnection.getConnectionIdentifier  ---  conectionIdentifier
        raise NotImplementedError()

    def sendUrlsForProcessing(self, url_list: list, max_workers: int, tokens: List[str] = None, delay: int = 1) -> utils.workerResponse.WorkerResponse:
        workers = self.reserveWorkers(max_workers)
        batches = self.prepareBatches(url_list, len(workers))

        threads = []
        results = [None] * len(workers)
        for wIdx in range(len(workers)):

            t = threading.Thread(target=self._innerThreadFunc, args=(workers[wIdx], batches[wIdx], results, wIdx,tokens,delay))
            threads.append(t)
            t.start()

        for t in threads:
            t.join()

        result = utils.workerResponse.WorkerResponse()

        for r in results:
            result.merge(r)

        for w in workers:
            w.releaseReservation()

        return result

    @staticmethod
    def _handleSingleWorker(w: server.workerConnection.WorkerConnection,
                            b: List[str],
                            tokens: List[str] = None,
                            delay: int = 1) -> utils.workerResponse.WorkerResponse:
        w.sendBatch(b, delay, tokens)
        resp = w.receiveResponse()
        return resp

    def _innerThreadFunc(self,
                         w: server.workerConnection.WorkerConnection,
                         b: List[str],
                         result_array: List[utils.workerResponse.WorkerResponse],
                         thread_id: int,
                         tokens: List[str] = None,
                         delay: int = 1):
        result_array[thread_id] = self._handleSingleWorker(w, b, tokens, delay)
        return

    def reserveWorkers(self, max_workers: int) -> List[server.workerConnection.WorkerConnection]:

        results = []
        for w in self._paired_workers:
            # w = server.workerConnection.WorkerConnection(w)
            if len(results) >= max_workers != -1:
                break
            if w.placeReservaion():
                results.append(w)
        return results
        # raise NotImplementedError()

    @staticmethod
    def prepareBatches(urls_to_divide: List[str], number_of_workers: int):
        number_per_worker = len(urls_to_divide) // number_of_workers
        remainder_for_lat_batch = len(urls_to_divide) - number_per_worker * number_of_workers

        batches = []

        for batch_number in range(number_of_workers):
            batches.append(urls_to_divide[batch_number * number_per_worker:(batch_number + 1) * number_per_worker])

        batches[-1] += urls_to_divide[number_per_worker * number_of_workers:]

        return batches

    def getNumWorkers(self) -> int:
        return len(self._paired_workers)

    # def sendSettingsToWorkers(self, workers: List[server.workerConnection.WorkerConnection], tokens: List[str] = None, delay: int = 1):
    #     for worker in workers:
    #         worker.setTokenList(tokens)
    #         worker.setDelay(delay)
