import copy
from collections import defaultdict
from datetime import datetime
from typing import List, Dict, Set

from utils.websiteMapGraph import WebsiteMapGraph
from utils.wordPhrasesVector import WordPhrasesVector
from utils.workerResponse import WorkerResponse


class JobResult:
    # credit_cards: List[str]
    # emails: List[str]
    website_map: WebsiteMapGraph
    # phone_numbers: List[str]
    # image_sources: List[str]
    # passwords: List[str]
    word_phrases: WordPhrasesVector
    # urls: List[str]
    # scripts: List[str]

    crawling_begun: datetime
    crawling_ended: datetime

    number_of_requests: int

    tokens: Dict[str, Set[str]]  # name of token; set of discovered tokens

    def __init__(self):
        # self.credit_cards = []
        # self.emails = []
        self.website_map = WebsiteMapGraph()
        # self.phone_numbers = []
        # self.image_sources = []
        # self.passwords = []
        self.word_phrases = WordPhrasesVector(4)
        # self.urls = []
        # self.scripts = []

        self.crawling_begun = None
        self.crawling_ended = None

        self.number_of_requests = 0

        self.tokens = defaultdict(lambda: set())

    def merge_with_worker_response(self, wr: WorkerResponse):
        self.website_map.merge(wr.website_map)
        self.word_phrases.merge(wr.wordPhrasesList)
        # self.tokens.update(wr.discovered_tokens)
        self.number_of_requests += wr.performed_requests

        for token_name in wr.discovered_tokens.keys():
            if token_name in self.tokens.keys():
                self.tokens[token_name] = \
                    self.tokens[token_name].union(wr.discovered_tokens[token_name])
            else:
                self.tokens[token_name] = set(copy.deepcopy(wr.discovered_tokens[token_name]))

    def __str__(self):
        result = "------- { JOB RESULT } ------- \n"
        result += "# Map: \n"
        result += str(self.website_map.getArcs())
        result += "\n# Word phrases: \n"
        result += str([dict(d) for d in self.word_phrases.getWords()])
        result += "\n# Tokens: \n"
        result += str(dict(self.tokens))
        return result

