from typing import List

import utils.workerResponse
from server.workerConnection import WorkerConnection
from worker.httpRequestInterface import httpRequestInterface
from worker.requestInterface import RequestInterface
from worker.workerMainRoutine import WorkerMainRoutine


class StandaloneConnection(WorkerConnection):
    worker: WorkerMainRoutine
    __next_batch: List[str]
    res: bool

    def __init__(self, request_interface: RequestInterface = httpRequestInterface()):
        self.worker = None
        self.__next_batch = []
        self.res = False
        self.__req_int = request_interface
        self.__delay = 1
        self.__tokens = None

    def sendBatch(self, batch: list, delay: int = 1, tokens: List[str] = None):
        self.__next_batch = batch
        self.__delay = delay
        self.__tokens = tokens

    def receiveResponse(self) -> utils.workerResponse.WorkerResponse:
        return self.worker.processBatch(self.__next_batch, self.__delay, self.__tokens)

    def pauseWorker(self):
        pass

    def reasumeWorker(self):
        pass

    def connectWorker(self, address, port, password):
        self.worker = WorkerMainRoutine(self.__req_int)
        pass

    def getConnectionIdentifier(self) -> int:
        pass

    def isAlive(self) -> bool:
        return True

    def releaseReservation(self) -> bool:
        """Returns true if reservation was released"""
        self.res = False
        return True

    def placeReservaion(self) -> bool:
        if not self.res:
            self.res = True
            return True
        return False

