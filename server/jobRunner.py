from server.job import Job
from server.jobRequest import JobRequest
from server.workerPool import WorkerPool
from server.jobResult import JobResult


class JobRunner:
    job: Job
    worker_pool: WorkerPool
    current_stage: int

    def __init__(self, req: JobRequest, wp: WorkerPool):
        self.job = Job(req)
        self.job.loadDataFromRequest()
        self.worker_pool = wp
        self.current_stage = 0

    def _nextStage(self):
        self.jobStage(self.current_stage)
        self.current_stage += 1
        self.job.setDepth(self.job.getDepth()+1)

    def jobStage(self, stage_id: int = 0):
        urls_to_process = self.job.getUrlsForDepth(stage_id)
        response = self.worker_pool.sendUrlsForProcessing(urls_to_process, self.job.getRequest().maxWorkers, self.job.getRequest().tokens_to_extract)
        self.job.consumeWorkerResponse(response, stage_id)

    def run(self) -> JobResult:
        while self.current_stage <= self.job.getRequest().requestedDepth:
            self._nextStage()
        return self.job.getResults()
