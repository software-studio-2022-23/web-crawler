from abc import ABC, abstractmethod
from typing import List

import utils.workerResponse


class WorkerConnection(ABC):

    @abstractmethod
    def sendBatch(self, batch: list, delay: int = 1, tokens: List[str] = None):
        pass

    @abstractmethod
    def receiveResponse(self) -> utils.workerResponse.WorkerResponse:
        pass

    @abstractmethod
    def pauseWorker(self):
        pass

    @abstractmethod
    def reasumeWorker(self):
        pass

    @abstractmethod
    def connectWorker(self, address, port, password):
        pass

    @abstractmethod
    def getConnectionIdentifier(self) -> int:
        pass

    @abstractmethod
    def isAlive(self) -> bool:
        pass

    @abstractmethod
    def placeReservaion(self) -> bool:
        """Function returns true if the worker becomes reserved for the job that requested it"""
        pass

    @abstractmethod
    def releaseReservation(self) -> bool:
        """Returns true if reservation was released"""
        pass

    # @abstractmethod
    # def setTokenList(self, tokens: List[str]):
    #     """Sets list of tokens to be extracted from future batches"""
    #     pass

    # @abstractmethod
    # def setDelay(self, delsy_in_sec: int):
    #     """Sets the delay between subsequent requests"""
    #     pass

    # Todo plugin registrtion
