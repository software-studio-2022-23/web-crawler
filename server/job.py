import copy
from typing import List

from server.jobResult import JobResult
from utils.workerResponse import WorkerResponse

from server.jobRequest import JobRequest


class Job:
    """Currently processed job with all needed data, created based on jobRequest"""
    _request: JobRequest
    _currentDepth: int
    _visitedUrls: set
    _processedWebsitesHashes: set
    _levelsUrls: List[List[str]]
    _result: JobResult

    def __init__(self, rq: JobRequest):
        self._request = rq
        self._currentDepth = 0
        self._visitedUrls = set()           # set of processed urls
        self._processedWebsitesHashes = set()   # if hash of website is in this set it was processed already - to avoid non canonical urls
        self._levelsUrls = []       # list in a form [[level 0 url 1, level 0 url 2 ...],[],[]...]
        self._result = JobResult()

    def loadDataFromRequest(self):
        self._levelsUrls = [[] for _ in range(self._request.requestedDepth+2)]
        self._levelsUrls[0] = self._request.seedUrls
        self._currentDepth = 0
        self._visitedUrls = set()
        self._processedWebsitesHashes = set()
        self._result = JobResult()

    def consumeWorkerResponse(self, response: WorkerResponse, current_depth):
        for u in response.discovered_urls:  # Adds urls to next level
            if u not in self._visitedUrls and u not in self._request.excludedUrls:
                self._levelsUrls[current_depth+1] += [u]
                self._visitedUrls.add(u)

        self._result.merge_with_worker_response(response)

    def getUrlsForDepth(self, depth: int):
        return copy.deepcopy(self._levelsUrls[depth])

    def getRequest(self) -> JobRequest:
        return copy.deepcopy(self._request)

    def getDepth(self):
        return self._currentDepth

    def setDepth(self, depth: int):
        self._currentDepth = depth

    def getResults(self) -> JobResult:
        return copy.deepcopy(self._result)
