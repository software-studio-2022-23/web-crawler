import copy
import time
from threading import Thread
from typing import List

from server.jobDataWrapper import JobDataWrapper
from server.jobRequest import JobRequest
from server.jobRunner import JobRunner
from server.workerConnection import WorkerConnection
from server.workerPool import WorkerPool


class Server:
    w_pool: WorkerPool
    requests: List[JobDataWrapper]
    running: bool
    loop_thread: Thread

    def __init__(self):
        self.w_pool = WorkerPool()
        self.requests = []
        self.running = False
        self.loop_thread = None

    def add_new_req(self, req: JobRequest):
        self.requests.append(JobDataWrapper(req))

    def get_job_requests(self) -> List[JobDataWrapper]:
        return copy.deepcopy(self.requests)

    def add_worker(self, w: WorkerConnection):
        self.w_pool.addWorker(w)

    def process_req(self, jdw:JobDataWrapper):
        jdw.processing_begun = True
        jr = JobRunner(jdw.job_request, self.w_pool)
        job_result = jr.run()
        jdw.job_result = job_result
        jdw.processing_ended = True

    def main_loop(self):
        print("Starting server loop")
        while self.running:
            for rq in self.requests:
                if not rq.processing_begun and not rq.processing_ended:
                    print("Starting processing job:", rq.job_request.jobName)
                    self.process_req(rq)
                    print(rq.job_result)
            time.sleep(0.3)

    def run(self):
        self.running = True
        self.loop_thread = Thread(target=self.main_loop)
        self.loop_thread.start()

    def get_job_by_name(self, name: str):
        for jdw in self.requests:
            if jdw.job_request.jobName == name:
                return copy.deepcopy(jdw)
        return None
