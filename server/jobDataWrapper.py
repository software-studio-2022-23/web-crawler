import copy
import datetime

from server.jobRequest import JobRequest
from server.jobResult import JobResult


class JobDataWrapper:
    creation_date: datetime.datetime
    job_request: JobRequest
    processing_begun: bool
    processing_ended: bool
    processing_canceled: bool
    job_result: JobResult

    def __init__(self, req: JobRequest):
        self.job_request = copy.deepcopy(req)
        self.creation_date = datetime.datetime.now()
        self.job_result = None
        self.processing_begun = False
        self.processing_ended = False
        self.processing_canceled = False
