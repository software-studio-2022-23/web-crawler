from typing import List


class JobRequest:
    tokens_to_extract: List[str]

    def __init__(self):
        self.seedUrls = []
        self.requestedDelay = 0 # in seconds
        self.requestedDepth = 0 # counting from 0 it specifies last level to be crawled
        self.excludedUrls = []
        self.jobName = ""
        self.maxWorkers = -1   # -1 means inf
        # self.highEntropyStrings = True
        # self.emails = True
        # self.phoneNumbers = True
        # self.js = True
        # self.imageSources = True
        # self.imageAltDesc = True
        # self.robotsTxt = True
        # self.sitemapXml = True
        # self.htmlComments = True

        # self._wordStatistics = True     #do count words

        # self.wordPhrases1 = True
        # self.wordPhrases2 = True
        # self.wordPhrases3 = True
        # self.wordPhrases4 = True

        self.tokens_to_extract = []

    def __str__(self):
        return "------- { JOB REQUEST } -------" + "\n" + \
               "Job name: " + self.jobName + "\n" + \
               "Seed urls: " + str(self.seedUrls) + "\n" + \
               "Requested delay [s]: " + str(self.requestedDelay) + "\n" + \
               "Last level to crawl (requested depth): " + str(self.requestedDepth) + "\n" + \
               "Excluded urls: " + str(self.excludedUrls) + "\n" + \
               "Max workers: " + str(self.maxWorkers) + "\n" + \
               "Tokens to extract: " + str(self.tokens_to_extract) + "\n"

