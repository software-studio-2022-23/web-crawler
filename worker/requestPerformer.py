import re
from typing import List
import datetime
import time

import requests
from bs4 import BeautifulSoup
import urllib.parse

from worker.extractor_plugins.charsets import SEPARATOR_DEFANGS, END_PUNCTUATION
from worker.httpRequestInterface import httpRequestInterface
from worker.requestInterface import RequestInterface


class RequestPerformer:
    minimal_delay: int
    __last_request: datetime
    __req_int: RequestInterface

    def __init__(self, req_int: RequestInterface = httpRequestInterface()):
        self.minimal_delay = 1
        self.__last_request = None
        self.__req_int = req_int

    def getContent(self, url: str) -> (str, List[str]):
        """

        :param url:
        :return: text of page with discovered urls
        """
        time_delay = datetime.timedelta(seconds=self.minimal_delay)
        while self.__last_request is not None and self.__last_request + time_delay >= datetime.datetime.now():
            time.sleep(1)

        self.__last_request = datetime.datetime.now()

        resp = self.__req_int.getWebsiteContent(url)
        soup = BeautifulSoup(resp, 'html.parser')

        link_selectors = soup.find_all("a")

        link_text_pattern = re.compile(r"""
        (
            # Scheme.
            [fhstu]\S\S?[px]s?
            # One of these delimiters/defangs.
            (?:
                :\/\/|
                :\\\\|
                :?__
            )
            # Any number of defang characters.
            (?:
                \x20|
                """ + SEPARATOR_DEFANGS + r"""
            )*
            # Domain/path characters.
            \w
            \S+?
            # CISCO ESA style defangs followed by domain/path characters.
            (?:\x20[\/\.][^\.\/\s]\S*?)*
        )
    """ + END_PUNCTUATION + r"""
        (?=\s|$)
    """, re.IGNORECASE | re.VERBOSE | re.UNICODE)
        link_in_text = link_text_pattern.findall(soup.get_text())
        discovered_urls = [] # [a['href'] for a in link_selectors]
        for a in link_selectors:
            if a.has_attr('href'):
                prepared_url = urllib.parse.urljoin(url, a['href'])
                if prepared_url not in discovered_urls:
                    discovered_urls.append(prepared_url)

        for link in link_in_text:
            if link not in discovered_urls:
                discovered_urls.append(link)

        return soup.get_text(), discovered_urls


    def setDelay(self, delay: int):
        self.minimal_delay = delay
