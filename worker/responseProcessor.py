from typing import List, Tuple, Dict, Set

from worker.rspProcPlugin import RspProcPlugin


class ResponseProcessor:
    plugins: Dict[str, RspProcPlugin]

    # str is the identifier of token, rspprocplugin is the extractor

    def __init__(self):
        self.plugins = {}

    def registerPlugin(self, token_name: str, pl: RspProcPlugin):
        self.plugins[token_name] = pl

    def process(self, tokens_to_retrieve: List[str], text: str) -> Dict[str, Set[str]]:
        result = {}
        for t in tokens_to_retrieve:
            if t in self.plugins.keys():
                result[t] = self.plugins[t].extract(text)
        return result
