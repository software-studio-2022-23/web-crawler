from typing import List

from worker import rspProcPlugin
from worker.httpRequestInterface import httpRequestInterface
from worker.pluginLoader import PluginLoader
from worker.requestInterface import RequestInterface
from worker.requestPerformer import RequestPerformer
from worker.responseProcessor import ResponseProcessor
from utils.workerResponse import WorkerResponse


class WorkerMainRoutine:
    __req_perf: RequestPerformer
    __resp_proc: ResponseProcessor
    __registered_tokens: List[str]

    def __init__(self, request_interface: RequestInterface = httpRequestInterface(), auto_register_default_plugins: bool = False):
        self.__req_perf = RequestPerformer(request_interface)
        self.__resp_proc = ResponseProcessor()
        self.__registered_tokens = []
        if auto_register_default_plugins:
            self.registerAllDefaultPlugins()

    def processBatch(self, urls: List[str], delay: int = 1, tokens: List[str] = None) -> WorkerResponse:
        result = WorkerResponse()
        for url in urls:
            result.merge(self.processSingleUrl(url, delay, tokens))
        return result

    def processSingleUrl(self, url: str, delay: int = 1, tokens: List[str] = None) -> WorkerResponse:
        # print("Processing:", url)
        result = WorkerResponse()

        self.__req_perf.setDelay(delay)
        content, list_of_urls = self.__req_perf.getContent(url)

        if tokens is not None:
            extracted_tokens = self.__resp_proc.process(tokens, content)
        else:
            extracted_tokens = self.__resp_proc.process(self.__registered_tokens, content)
        result.discovered_tokens = extracted_tokens
        result.discovered_urls = list_of_urls
        result.performed_requests = 1
        result.wordPhrasesList.consumeString(content)
        for child in list_of_urls:
            result.website_map.addNewArc(url, child)

        return result

    def registerPluginForToken(self, token_name: str, plugin: rspProcPlugin):
        self.__registered_tokens.append(token_name)
        self.__resp_proc.registerPlugin(token_name, plugin)


    def registerAllDefaultPlugins(self):
        self.__registered_tokens += PluginLoader().loadAllDefaultPlugins(self.__resp_proc)
