import requests

from worker.requestInterface import RequestInterface


class httpRequestInterface(RequestInterface):
    def getWebsiteContent(self, url) -> str:
        resp = requests.get(url)
        return resp.text
