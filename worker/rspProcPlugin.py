from abc import ABC, abstractmethod
from typing import TypeVar, Generic, Set

T = TypeVar('T')


class RspProcPlugin(ABC, Generic[T]):

    @abstractmethod
    def extract(self, text: str) -> Set[T]:
        pass
