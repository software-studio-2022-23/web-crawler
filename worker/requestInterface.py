from abc import ABC, abstractmethod
from typing import Union


class RequestInterface(ABC):

    @abstractmethod
    def getWebsiteContent(self, url) -> Union[str, bytes]:
        """
        Function returns content of the website as a string
        :param url:
        :return:
        """
        pass
