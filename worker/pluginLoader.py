from worker.extractor_plugins.email import EMAIL_plugin
from worker.extractor_plugins.ipv4 import IPV4_plugin
from worker.responseProcessor import ResponseProcessor


class PluginLoader:
    def loadAllDefaultPlugins(self, rsp_proc: ResponseProcessor):
        rsp_proc.registerPlugin("IPv4", IPV4_plugin())
        rsp_proc.registerPlugin("EMAIL", EMAIL_plugin())
        return ["IPv4", "EMAIL"]
