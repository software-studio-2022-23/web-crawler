import re
from abc import ABC, abstractmethod
from typing import Set

from worker import rspProcPlugin
from worker.rspProcPlugin import RspProcPlugin, T


class RegexBasedRspProcPlugin(RspProcPlugin[str], ABC):
    pt: re.Pattern

    def __init__(self):
        self.pt = self.__createRegex()

    @abstractmethod
    def __createRegex(self) -> re.Pattern:
        pass

    def extract(self, text: str) -> Set[str]:
        return set(self.pt.findall(text))

