from typing import Set

import re

from worker.extractor_plugins.charsets import END_PUNCTUATION, SEPARATOR_DEFANGS
from worker.regexBasedRspProcPlugin import RegexBasedRspProcPlugin


class EMAIL_plugin(RegexBasedRspProcPlugin):

    # def __init__(self):
    #     super().__init__()

    def _RegexBasedRspProcPlugin__createRegex(self) -> re.Pattern:
        return re.compile(r"""
        (
            [a-z0-9_.+-]+
            [\(\[{\x20]*
            (?:@|\Wat\W)
            [\)\]}\x20]*
            [a-z0-9-]+
            (?:
                (?:
                    (?:
                        \x20*
                        """ + SEPARATOR_DEFANGS + r"""
                        \x20*
                    )*
                    \.
                    (?:
                        \x20*
                        """ + SEPARATOR_DEFANGS + r"""
                        \x20*
                    )*
                    |
                    \W+dot\W+
                )
                [a-z0-9-]+?
            )+
        )
    """ + END_PUNCTUATION + r"""
        (?=\s|$)
    """, re.IGNORECASE | re.VERBOSE | re.UNICODE)

