from typing import Set

import re

from worker.regexBasedRspProcPlugin import RegexBasedRspProcPlugin
from worker.rspProcPlugin import RspProcPlugin


class IPV4_plugin(RegexBasedRspProcPlugin):

    # def __init__(self):
    #     super().__init__()

    def _RegexBasedRspProcPlugin__createRegex(self) -> re.Pattern:
        return re.compile(r"""
                (?:^|
                    (?![^\d\.])
                )
                (?:
                    (?:[1-9]?\d|1\d\d|2[0-4]\d|25[0-5])
                    [\[\(\\]*?\.[\]\)]*?
                ){3}
                (?:[1-9]?\d|1\d\d|2[0-4]\d|25[0-5])
                (?:(?=[^\d\.])|$)
            """, re.VERBOSE)

