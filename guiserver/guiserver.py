import flask
import pdfkit
from flask import Response

from guiserver import reportGenerator
from server.jobDataWrapper import JobDataWrapper
from server.jobRequest import JobRequest
from server.server import Server

serverapp = flask.Flask(__name__)
serverapp.secret_key = "ncgi83ct2345udtho234htmoi34hmxrh13nioruix2ruic34hmt43moictmi3WEHBUun6lW$^BUNI5m7"
srv: Server = None

@serverapp.route("/", methods=["POST", "GET"])
def main_route():
    if flask.request.method == "POST":
        if flask.request.form["username"] == "user" and flask.request.form["password"] == "password1234":
            flask.session["logged_in"] = True

    if "logged_in" in flask.session and flask.session["logged_in"]:
        return flask.render_template("gui.html", data=srv.requests)
    return flask.render_template("login.html")


@serverapp.route("/logout", methods=["GET"])
def logout():
    flask.session.pop('logged_in', None)
    return flask.redirect(flask.url_for('main_route'))


@serverapp.route("/new_task", methods=["POST","GET"])
def new_task():
    if "logged_in" not in flask.session or not flask.session["logged_in"]:
        return flask.redirect(flask.url_for('main_route'))

    if flask.request.method == "GET":
        return flask.render_template("task.html")

    if flask.request.method == "POST":
        # process request
        jr = JobRequest()
        jr.jobName = flask.request.form["name"]
        jr.seedUrls = [flask.request.form["url_seed"]]
        jr.requestedDepth = int(flask.request.form["depth"])
        jr.requestedDelay = int(flask.request.form["delay"])
        jr.maxWorkers = int(flask.request.form["max_work"])
        if flask.request.form["excl_url"] != "":
            jr.excludedUrls = [flask.request.form["excl_url"]]
        if "option7" in flask.request.form and bool(flask.request.form["option7"]):
            jr.tokens_to_extract += ["EMAIL"]
        if "option16" in flask.request.form and bool(flask.request.form["option16"]):
            jr.tokens_to_extract += ["IPV4"]
        srv.add_new_req(jr)
        return flask.redirect(flask.url_for('main_route'))


@serverapp.route("/chart", methods=["GET"])
def chart():
    if "logged_in" not in flask.session or not flask.session["logged_in"]:
        return flask.redirect(flask.url_for('main_route'))
    return flask.render_template("chart.html")


@serverapp.route("/new_worker", methods=["POST", "GET"])
def new_worker():
    if "logged_in" not in flask.session or not flask.session["logged_in"]:
        return flask.redirect(flask.url_for('main_route'))

    if flask.request.method == "GET":
        return flask.render_template("worker.html")

    if flask.request.method == "POST":
        # process request
        return flask.redirect(flask.url_for('main_route'))


@serverapp.route("/download_report", methods=["POST", "GET"])
def download_report():
    if "job_id" in flask.request.args:
        jdw = srv.get_job_by_name(flask.request.args["job_id"])
        if jdw is not None:
            pdf = create_pdf_report(jdw)
            return Response(pdf, mimetype="application/pdf")
            # return flask.send_file(pdf, as_attachment=True, mimetype='application/pdf', download_name='report.pdf', max_age=0)
    return flask.redirect(flask.url_for('main_route'))

# @serverapp.route("/", )
# def process_login():
#     return flask.render_template("login.html")

def run_flask(debug: bool, static_folder, server: Server):
    serverapp.static_folder = static_folder
    global srv
    srv = server
    serverapp.run(debug=debug)


def create_pdf_report(data_wrapper: JobDataWrapper):
    path_wkhtmltopdf = r'guiserver/wkhtmltox/wkhtmltopdf.exe'
    config = pdfkit.configuration(wkhtmltopdf=path_wkhtmltopdf)
    # Create a new PDF document
    report_data = [data_wrapper.creation_date, data_wrapper.processing_begun, data_wrapper.processing_ended, data_wrapper.job_request, data_wrapper.job_result]

    out = flask.render_template("report.html", data=report_data)
    options = {
        "orientation": "landscape",
        "page-size": "A4",
        "margin-top": "1.0cm",
        "margin-right": "1.0cm",
        "margin-bottom": "1.0cm",
        "margin-left": "1.0cm",
        "encoding": "UTF-8",
    }

    # Build PDF from HTML
    pdf = pdfkit.from_string(out, options=options, configuration=config)
    return pdf
if __name__ == '__main__':
    run_flask(True, 'static')

