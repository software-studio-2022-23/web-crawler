from io import BytesIO

import pdfkit
from server.jobDataWrapper import JobDataWrapper
from reportlab.pdfgen import canvas


def create_pdf_report(data_wrapper: JobDataWrapper):

    # Create a new PDF document
    report_data = [data_wrapper.creation_date, data_wrapper.processing_begun, data_wrapper.processing_ended, data_wrapper.job_request, data_wrapper.job_result]

    options = {
        "orientation": "landscape",
        "page-size": "A4",
        "margin-top": "1.0cm",
        "margin-right": "1.0cm",
        "margin-bottom": "1.0cm",
        "margin-left": "1.0cm",
        "encoding": "UTF-8",
    }

    # Build PDF from HTML
    pdf = pdfkit.from_string(out, options=options, configuration=config)

    return buffer
