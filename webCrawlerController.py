from server.jobRequest import JobRequest
from server.jobResult import JobResult
from server.jobRunner import JobRunner
from server.standaloneConnection import StandaloneConnection
from server.workerConnection import WorkerConnection
from server.workerPool import WorkerPool


class WebCrawlerController:
    __wp: WorkerPool

    def __init__(self):
        self.__wp = WorkerPool()

    def createStandaloneWorker(self):
        worker = StandaloneConnection()
        worker.connectWorker("", "", "")
        self.__wp.addWorker(worker)

    def addWorker(self, w: WorkerConnection):
        self.__wp.addWorker(w)

    def runRequest(self, jreq: JobRequest) -> JobResult:
        jr = JobRunner(jreq, self.__wp)
        return jr.run(jreq)
